resource "aws_instance" "web" {
  ami             = "ami-08ca3fed11864d6bb"
  instance_type   = "t2.micro"
  key_name        = var.keyname
  subnet_id       = aws_subnet.privatesubnets.id
  security_groups = [aws_security_group.sg_web.id]

  user_data = <<-EOF
  #!/bin/bash
  echo "*** Installing apache2"
  sudo apt update -y
  sudo apt install apache2 -y
  sudo systemctl status apache2
  echo "*** Completed Installing apache2
  echo "Experiment Project" > /home/ubuntu/index.html"
  EOF

  tags = {
    Name = "web_instance"
  }

  volume_tags = {
    Name = "web_instance"
  }
}

resource "aws_instance" "bastion" {
  ami             = "ami-08ca3fed11864d6bb"
  instance_type   = "t2.micro"
  key_name        = var.keyname
  subnet_id       = aws_subnet.publicsubnets.id
  security_groups = [aws_security_group.sg_bastion.id]

  tags = {
    Name = "bastion_instance"
  }

  volume_tags = {
    Name = "bastion_instance"
  }
}

resource "aws_eip" "lb" {
  instance = "${aws_instance.bastion.id}"
  vpc      = true
}

resource "aws_security_group" "sg_bastion" {
  name        = "allow_ssh_http_bastion"
  description = "Allow ssh http inbound traffic"
  vpc_id      = aws_vpc.Main.id

  ingress {
    description      = "SSH from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "HTTP from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}

resource "aws_security_group" "sg_web" {
  name        = "allow_ssh_http_web"
  description = "Allow ssh http inbound traffic"
  vpc_id      = aws_vpc.Main.id
  ingress {
    description      = "SSH from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}