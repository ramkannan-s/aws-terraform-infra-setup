provider "aws" {
  region                  = var.region
  shared_credentials_file = var.creds
  profile                 = "default"
}
/*terraform {
  backend "s3" {
    bucket         = "experiment_bucket_1"
    key            = "terraform.tfstate"
    region         = "eu-west-1"
  }
}*/

