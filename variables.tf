variable "region" {
  default = "eu-west-1"
}

variable "creds" {
  default = "~/.aws/credentials"
}

variable "main_vpc_cidr" {
  default = "10.155.155.0/24"
}

variable "main_vpc_id" {
  default = "experiment-vpc"
}

variable "private_subnets" {
  default = "10.155.155.0/25"
}

variable "public_subnets" {
  default = "10.155.155.128/25"
}

variable "keyname" {
  default = "experiment-keypair"
}

